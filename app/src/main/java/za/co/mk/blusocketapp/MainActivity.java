package za.co.mk.blusocketapp;

import android.os.Bundle;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import za.co.mk.blusocketapp.network.TCPClient;
import za.co.mk.blusocketapp.network.TCPClientLoader;

public class MainActivity extends AppCompatActivity implements android.support.v4.app.LoaderManager.LoaderCallbacks<String> {

    private TCPClient tcpClient;
    public static final String SERVER_IP = "196.37.22.179"; //your computer IP address
    public static final int SERVER_PORT = 9011;

    public static final String SERVER_TEXT_RESPONSE_KEY = "serverTextResponseKey";

    private ProgressBar loadingProgress;
    private TextView serverMessageText;
    private Button authButton;
    private int loaderId = 1;
    private String rawServerData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        loadingProgress = findViewById(R.id.loading);
        serverMessageText = findViewById(R.id.serverMessage);
        authButton = findViewById(R.id.authButton);

        authButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadingProgress.setVisibility(View.VISIBLE);
                getSupportLoaderManager().initLoader(loaderId, null, MainActivity.this);
            }
        });

        if (savedInstanceState != null) {
            // handling config change - only show if there is rawServerData to show, otherwise user
            // will hit auth button
            rawServerData = savedInstanceState.getString(SERVER_TEXT_RESPONSE_KEY);
            serverMessageText.setText(rawServerData);
        }

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(SERVER_TEXT_RESPONSE_KEY, rawServerData);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        rawServerData = savedInstanceState.getString(SERVER_TEXT_RESPONSE_KEY);
    }

    @Override
    public Loader<String> onCreateLoader(int id, Bundle args) {
        return new TCPClientLoader(this, SERVER_IP, SERVER_PORT, getMessageToSend());
    }

    private String getMessageToSend() {
        //TODO rather get the string in a more configurable way
        return "<request><EventType>Authentication</EventType><event><DeviceVer>ABCDE</DeviceVer><UserPin>12345</UserPin><DeviceId>12345</DeviceId><DeviceSer>ABCDE</DeviceSer><TransType>Users</TransType></event></request>";
    }

    @Override
    public void onLoadFinished(Loader<String> loader, String data) {
        rawServerData = data;
        loadingProgress.setVisibility(View.GONE);
        serverMessageText.setText(rawServerData);

    }

    @Override
    public void onLoaderReset(Loader<String> loader) {

    }


}
