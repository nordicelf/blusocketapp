package za.co.mk.blusocketapp.utils;

import android.util.Log;

/**
 * Created by michelle on 2017/11/05.
 * A class to log to logcat - will only log in debug mode
 */

public class AppLogger {

    public static void info(String message) {
        if (AppConfig.DEBUG) {
            Log.i(AppConfig.LOG_TAG, message);

        }
    }
}
