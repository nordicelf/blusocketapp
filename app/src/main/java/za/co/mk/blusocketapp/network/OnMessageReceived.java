package za.co.mk.blusocketapp.network;

/**
 * Created by michelle on 2017/11/04.
 */

public interface OnMessageReceived {
    public void messageReceived(String message);
}
