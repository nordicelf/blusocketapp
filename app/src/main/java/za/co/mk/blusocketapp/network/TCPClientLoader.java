package za.co.mk.blusocketapp.network;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;

import za.co.mk.blusocketapp.utils.AppLogger;

/**
 * Created by michelle on 2017/11/05.
 *
 * An AsyncTaskLoader to call the TCP Client
 */

public class TCPClientLoader extends AsyncTaskLoader<String> {
    private final String serverIp;
    private final int serverPort;
    private final String message;
    String serverMessage;

    public TCPClientLoader(Context context, String serverIp, int serverPort, String message) {
        super(context);
        this.serverIp = serverIp;
        this.serverPort = serverPort;
        this.message = message;
    }

    @Override
    protected void onStartLoading() {
        if (serverMessage != null) {
            deliverResult(serverMessage);
            AppLogger.info("serverMessage != null");
        } else {
            forceLoad();
        }
    }

    @Override
    public String loadInBackground() {

        TCPClient tcpClient = new TCPClient(serverIp, serverPort);
        return tcpClient.run(message);
    }

    @Override
    public void deliverResult(String data) {
        AppLogger.info("deliverResult "+data);
        serverMessage = data;
        super.deliverResult(data);
    }
}


