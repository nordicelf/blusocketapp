package za.co.mk.blusocketapp.network;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;

import za.co.mk.blusocketapp.utils.AppLogger;

/**
 * Created by michelle on 2017/11/03.
 * <p>
 * Very, very basic TCP Client class - this would ideally need some security and other features.
 */

public class TCPClient {


    private String serverAddress;
    private final int serverPort;
    // message to send to the server
    private String serverMessage;
    // used to send messages
    private PrintWriter bufferOut;
    // used to read messages from the server
    private BufferedReader bufferIn;


    /**
     * Constructor
     *
     * @param serverAddress
     * @param serverPort
     */
    public TCPClient(String serverAddress, int serverPort) {
        this.serverAddress = serverAddress;
        this.serverPort = serverPort;
    }

    /**
     * Sends the message from client to the server
     *
     * @param message client text
     */
    public void sendMessage(String message) {
        AppLogger.info("sendMessage " + message);
        if (bufferOut != null && !bufferOut.checkError()) {
            bufferOut.println(message);
            bufferOut.flush();
        }
    }

    /**
     * Cleans up writing
     */
    public void stopClient() {

        if (bufferOut != null) {
            bufferOut.flush();
            bufferOut.close();
        }

    }

    public String run(String message) {


        try {
            //IP address
            InetAddress serverAddr = InetAddress.getByName(serverAddress);


            //create a socket
            Socket socket = new Socket(serverAddr, serverPort);

            try {

                //sends the message to the server
                bufferOut = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())), true);

                //receives the message from the server
                bufferIn = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                // send
                sendMessage(message);
                serverMessage = bufferIn.readLine();


            } catch (Exception e) {
                AppLogger.info("Socket Exception: " + e.getMessage());

            } finally {
                //close the socket
                socket.close();
                stopClient();
            }

        } catch (Exception e) {

            AppLogger.info("Error " + e.getMessage());

        }

        return serverMessage;
    }
}
