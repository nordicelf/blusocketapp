package za.co.mk.blusocketapp.utils;

import za.co.mk.blusocketapp.BuildConfig;

/**
 * Created by michelle on 2017/11/05.
 * Basic app configurations go here
 */

public class AppConfig {
    public static final String LOG_TAG = "BluSocketApp";
    // Is only in Debug mode when BuildConfig is in debug - won't accidentally be in debug in release
    public static final boolean DEBUG = (BuildConfig.DEBUG);

}
